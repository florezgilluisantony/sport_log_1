/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLER;
import MODEL.Minventario;
import VIEW.JFinventario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author USER04
 */
public class Cinventario implements ActionListener{
    
    private final JFinventario vinventario;
    private final Minventario minventario;

    public Cinventario(JFinventario vinventario, Minventario minventario) {
        
   
        this.vinventario = vinventario;
        this.minventario = minventario;
        
        this.vinventario.JBguardar.addActionListener(this);
    }
    
    

    @Override
    public void actionPerformed(ActionEvent e) {
        
        String txtnombre = "";
        String txtfecha = "";
        String txtcantidad = "";
        String txtestado = "";
        
        if (this.vinventario.txtnombre.getText().equals("")) {
           txtnombre= "";            
        } else {
            txtnombre = this.vinventario.txtnombre.getText();}
        
        if (this.vinventario.txtfecha.getText().equals("")) {
            txtfecha = "";            
        } else {
            txtfecha = this.vinventario.txtfecha.getText();}
        
        if (this.vinventario.txtcantidad.getText().equals("")) {
            txtcantidad = "";            
        } else {
            txtcantidad  = this.vinventario.txtcantidad.getText();}
        
         if (this.vinventario.txtestado.getText().equals("")) {
            txtestado = "";            
        } else {
            txtestado  = this.vinventario.txtestado.getText();}
    
    
       if (e.getSource() == this.vinventario.JBguardar){
       

            this.minventario.setNombre(txtnombre);
            this.minventario.setFecha(txtfecha);
            this.minventario.setCantidad_de_articulos(txtcantidad);
            this.minventario.setEstado(txtestado);
            
            if (this.minventario.guardar()) {
                JOptionPane.showMessageDialog(null, "Datos Guardado Con Exitos :) ");
            } else {
                JOptionPane.showMessageDialog(null, "Datos No Guardado :(");
            } 
          }
       }
}
