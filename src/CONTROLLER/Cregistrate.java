
package CONTROLLER;

import MODEL.Mlogin;
import MODEL.Mregistrate;
import VIEW.JFlogin;
import VIEW.JFregistrate;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;

public class Cregistrate implements ActionListener, KeyListener, MouseListener  {
    
    private final JFregistrate vregistrate;
    private final Mregistrate mregistrate;

    public Cregistrate(JFregistrate vregistrate, Mregistrate mregistrate) {
        this.vregistrate = vregistrate;
        this.mregistrate = mregistrate;

        this.vregistrate.Btnguardar.addActionListener(this);
        this.vregistrate.Btnlimpiar.addActionListener(this);
        this.vregistrate.Btnatras.addActionListener(this);
        this.vregistrate.txtNombre.addKeyListener(this);
        this.vregistrate.txtdocumento.addKeyListener(this);
        this.vregistrate.txtcontraseña1.addKeyListener(this);
        this.vregistrate.txtcontraseña2.addKeyListener(this);
        this.vregistrate.JTUsuario.addKeyListener(this);
        this.vregistrate.txtTipo_de_cargo.addKeyListener(this);       
    }
    
    public void iniciar (){
        this.vregistrate.setLocationRelativeTo(null);
        this.vregistrate.setVisible(true);
    }
    
    
   

    @Override
    public void actionPerformed(ActionEvent e) {
        
         if (e.getSource() == this.vregistrate.Btnatras) {
                JFlogin  LOGIN = new  JFlogin();
                Mlogin mlogin = new Mlogin();
                Clogin clogin = new Clogin(LOGIN, mlogin);
                LOGIN.setVisible(true);
                this.dispose();
        }
        
        String Usuario = "";
        String Nombre = "";
        String Tipo_de_cargo = "";
        int Numero_de_documento = 0;
        String Contraseña1 = "";
        String Contraseña2 = "" ;
               
         if (this.vregistrate.JTUsuario.getText().equals("")) {
            Usuario = "";
        } else {
            Usuario = this.vregistrate.JTUsuario.getText();
        }       
        if (this.vregistrate.txtNombre.getText().equals("")) {
            Nombre = "";
        } else {
            Nombre = this.vregistrate.txtNombre.getText();
        }        
        if (this.vregistrate.txtTipo_de_cargo.getText().equals("")) {
            Tipo_de_cargo = "";
        } else {
            Tipo_de_cargo = this.vregistrate.txtTipo_de_cargo.getText();
        }
        if (this.vregistrate.txtdocumento.getText().equals("")) {
           Numero_de_documento = 0;
        } else {
           Numero_de_documento = Integer.parseInt(this.vregistrate.txtdocumento.getText());
        }
        if (this.vregistrate.txtcontraseña1.getText().equals("")) {
            Contraseña1 = "";
        } else {
            Contraseña1 = this.vregistrate.txtcontraseña1.getText();
        }
        if (this.vregistrate.txtcontraseña2.getText().equals("")) {
            Contraseña2 = "";
        } else {
            Contraseña2 = this.vregistrate.txtcontraseña2.getText();
        }
              
         if (e.getSource() == this.vregistrate.Btnguardar) {

           
            this.mregistrate.setNombre(Nombre);
            this.mregistrate.setTipo_de_cargo(Tipo_de_cargo);
            this.mregistrate.setNumero_de_documento(Numero_de_documento);
            this.mregistrate.setUsuario(Usuario);
            this.mregistrate.setContraseña1(Contraseña1);
            this.mregistrate.setContraseña2(Contraseña2);

            if (this.mregistrate.Guardar()) {
                JOptionPane.showMessageDialog(null, "Datos Guardado Con Exitos :) ");
            } else {
                JOptionPane.showMessageDialog(null, "Datos No Guardado :(");
            }
            
            
           
            
        
    }
    
}

 

    @Override
    public void keyTyped(KeyEvent e) {
        
       
        
    }

    @Override
    public void keyPressed(KeyEvent e) {
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
       
    }

    @Override
    public void mouseClicked(MouseEvent e) {
       
    }

    @Override
    public void mousePressed(MouseEvent e) {
        
                }

    @Override
    public void mouseReleased(MouseEvent e) {
      
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        
    }

    @Override
    public void mouseExited(MouseEvent e) {
       
    }

    private void dispose() {
        
    }
}
