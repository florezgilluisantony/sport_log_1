/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLER;

import MODEL.Masistencia;
import MODEL.Minventario;
import MODEL.Mmenu;
import MODEL.Mregistro_de_jugadores;
import MODEL.Mrendimiento_fisico;
import VIEW.JFasistencia;
import VIEW.JFinventario;
import VIEW.JFmenu;
import VIEW.JFregistro_de_jugadores;
import VIEW.JFrendimiento_fisico;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

/**
 *
 * @author USER04
 */
public class Cmenu implements ActionListener {
    
    private JFmenu vmenu;
    private Mmenu menu;

    public Cmenu(JFmenu vmenu, Mmenu menu) {
        this.vmenu = vmenu;
        this.menu = menu;
        this.vmenu.JBAsistencia1.addActionListener(this);
        this.vmenu.JBInventario.addActionListener(this);
        this.vmenu.JBRegistro_jugador.addActionListener(this);
        this.vmenu.JBRendimiento.addActionListener(this);
        this.vmenu.JBcerrarsecion.addActionListener(this);
    
    }
    
    private void cerrar(){
        String botones[] = {"cerrar","cancelar"};
        int eleccion;
             eleccion = JOptionPane.showOptionDialog(null, "¿desea cerrar secion?", "titulo",
               0, 0, null, botones, this);
        if(eleccion==JOptionPane.YES_OPTION){
            System.exit(0);
        }else if(eleccion==JOptionPane.NO_OPTION){
        System.out.print("se cancelo el cierre de secion");
     }
   }
    
    

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() ==this.vmenu.JBAsistencia1 ) {
            
            JFasistencia va =  new JFasistencia ();
            Masistencia ma = new Masistencia ();
            Casistencia ca =  new Casistencia (va, ma);
            va.setVisible(true);
            va.setLocationRelativeTo(null);
            this.dispose();
        }
        
       if (e.getSource() ==this.vmenu.JBInventario ) {
            
            JFinventario va =  new JFinventario();
            Minventario ma = new Minventario();
            Cinventario ca =  new Cinventario(va, ma);
            va.setVisible(true);
            va.setLocationRelativeTo(null);
            this.dispose();           
        }
       
       if (e.getSource() ==this.vmenu.JBRendimiento ) {
            
            JFrendimiento_fisico va =  new JFrendimiento_fisico();
            Mrendimiento_fisico ma = new Mrendimiento_fisico();
            Crendimiento_fisico ca =  new Crendimiento_fisico(va, ma);
            va.setVisible(true);
            va.setLocationRelativeTo(null);
            this.dispose();
        }
       if (e.getSource() ==this.vmenu.JBRegistro_jugador ) {
            
            JFregistro_de_jugadores va =  new JFregistro_de_jugadores();
            Mregistro_de_jugadores ma = new Mregistro_de_jugadores();
            Cregistro_de_jugadores ca =  new Cregistro_de_jugadores(va, ma);
            va.setVisible(true);
            va.setLocationRelativeTo(null);
            this.dispose();
        }
       
        if (e.getSource() ==this.vmenu.JBcerrarsecion) {
            
            cerrar();
            
        }
       
    }

    private void dispose() {
       
    }
    
    
    
    
}
