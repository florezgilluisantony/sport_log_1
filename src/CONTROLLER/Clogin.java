/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLER;

import MODEL.Mlogin;
import MODEL.Mmenu;
import MODEL.Mregistrate;
import VIEW.JFlogin;
import VIEW.JFmenu;
import VIEW.JFregistrate;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;


/**
 *
 * @author USER04
 */
public class Clogin implements  ActionListener, MouseListener {
    
    private JFlogin vlogin;
    private Mlogin mlogin;
    
    
    public Clogin (JFlogin vlogin, Mlogin mlogin){
        
    this.vlogin = vlogin;
    this.mlogin = mlogin;
    this.vlogin.txtUsuario.addActionListener(this);
    this.vlogin.txtPassword.addActionListener(this);
    this.vlogin.BtnIniciarsesion.addActionListener(this);
    this.vlogin.BtnRegistrese.addActionListener(this);
    this.vlogin.BtnSalir.addActionListener(this);
}
    public void iniciar(){
        
        this.vlogin.setLocationRelativeTo(null);
        this.vlogin.setVisible(true); 
    }
    

    private void cerrar(){
     String botones[] = {"cerrar","cancelar"};
     int eleccion;
       eleccion = JOptionPane.showOptionDialog(null,"¿desea cerrar esta ventana?", "titulo",
               0, 0, null, botones, this);
     if(eleccion==JOptionPane.YES_OPTION){
         System.exit(0);
     }else if(eleccion==JOptionPane.NO_OPTION){
      System.out.print("se cancelo el cierre");
     }
   }
          
    
    
  

    @Override
    public void actionPerformed(ActionEvent e) {
        
        
        if (e.getSource() == this.vlogin.BtnIniciarsesion) {
            
        char clave [] = vlogin.txtPassword.getPassword();  
        
          String clavedef = new String(clave);

          if (vlogin.txtUsuario.getText().equals("cristian") && clavedef.equals("12345")){
      
            JOptionPane.showMessageDialog(null, "Bienvenido, Has ingresado "
              + "satisfactoriamente al sistema", "Mensaje de bienvenida",
              JOptionPane.INFORMATION_MESSAGE);    
     
                 JFmenu jm = new JFmenu();
                 Mmenu mm = new Mmenu();
                 Cmenu cm = new Cmenu(jm, mm);
                 jm.setLocationRelativeTo(null);
                 jm.setVisible(true);
                 this.dispose();
                 
             }else { 
              
            if (vlogin.txtUsuario.getText().isEmpty() || clavedef.isEmpty()) {
          
              JOptionPane.showMessageDialog(null, "Acceso denegado:\n"
                + "Por favor ingrese un usuario y contraseña correctos",
                "Acceso denegado", JOptionPane.ERROR_MESSAGE);}     
            }
        if (e.getSource() == this.vlogin.BtnRegistrese) {
                JFregistrate vre = new   JFregistrate();
                Mregistrate mre = new Mregistrate();
                Cregistrate cre = new Cregistrate(vre, mre);
                vre.setVisible(true);
                vre.setLocationRelativeTo(null);
                this.dispose();
        }
        
        if (e.getSource() == this.vlogin.BtnSalir) {
            cerrar();
            
        }
        
        
        
        }
        
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        
        
    }

    @Override
    public void mousePressed(MouseEvent e) {
       
    }

    @Override
    public void mouseReleased(MouseEvent e) {
  
    }

    @Override
    public void mouseEntered(MouseEvent e) {
 
    }

    @Override
    public void mouseExited(MouseEvent e) {
        
          }

    private void dispose() {
        
          }
    
    
    
    
}
