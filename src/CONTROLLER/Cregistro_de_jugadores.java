/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CONTROLLER;

import MODEL.Mmenu;
import MODEL.Mregistro_de_jugadores;
import VIEW.JFmenu;
import VIEW.JFregistro_de_jugadores;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JOptionPane;

/**
 *
 * @author USER04
 */
public class Cregistro_de_jugadores implements ActionListener,KeyListener {
    
    private JFregistro_de_jugadores vregistro;
    private Mregistro_de_jugadores mregistro;

    public Cregistro_de_jugadores(JFregistro_de_jugadores vregistro, Mregistro_de_jugadores mregistro) {
        this.vregistro = vregistro;
        this.mregistro = mregistro;
        this.vregistro.JBactualizar.addActionListener(this);
        this.vregistro.JBbuscar.addActionListener(this);
        this.vregistro.JBeliminar.addActionListener(this);
        this.vregistro.JBlimpiar.addActionListener(this);
        this.vregistro.JBguardar.addActionListener(this);
        this.vregistro.JBatras.addActionListener(this);
        this.vregistro.txtnombre.addKeyListener(this);
        this.vregistro.txtapellido.addKeyListener(this);
        this.vregistro.boxtipo_de_identificacion.addKeyListener(this);
        this.vregistro.txtnumero_de_identificacion.addKeyListener(this);
        this.vregistro.txtedad.addKeyListener(this);
        this.vregistro.txteps.addKeyListener(this);
        this.vregistro.boxsexo.addKeyListener(this);
        this.vregistro.boxposicion_de_juego.addKeyListener(this);
        this.vregistro.box_categoria.addKeyListener(this);
    }
    
    private void cerrar(){
     String botones[] = {"cerrar","cancelar"};
     int eleccion;
       eleccion = JOptionPane.showOptionDialog(null, "¿Desea volver al menu?", "titulo",
               0, 0, null, botones, this);
     if(eleccion==JOptionPane.YES_OPTION){
          JFmenu jm = new JFmenu();
                 Mmenu mm = new Mmenu();
                 Cmenu cm = new Cmenu(jm, mm);
                 jm.setLocationRelativeTo(null);
                 jm.setVisible(true);
                 this.dispose();
     }else if(eleccion==JOptionPane.NO_OPTION){
      System.out.print("se cancelo el cierre de secion");
     }
   }
    

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() ==this.vregistro.JBatras) {
            
            cerrar();
            
        }
        
        
        
        
       
    }

    @Override
    public void keyTyped(KeyEvent e) {
        
        if (e.getSource() == this.vregistro.txtnumero_de_identificacion) {
            char validar = e.getKeyChar();
            if (Character.isLetter(validar)){
                this.vregistro.txtnumero_de_identificacion.getToolkit().beep();
                e.consume();
                JOptionPane.showMessageDialog(null, "Digite Solo  Número Por Favor ");   
            }
         }
        if (e.getSource() == this.vregistro.txtedad) {
            char validar = e.getKeyChar();
            if (Character.isLetter(validar)){
                this.vregistro.txtedad.getToolkit().beep();
                e.consume();
                JOptionPane.showMessageDialog(null, "Digite Solo  Número Por Favor ");   
            }
        }
        
        if (e.getSource() == this.vregistro.txtnombre)  {
            char validar = e.getKeyChar();
            if (Character.isDigit(validar)) {
                this.vregistro.txtnombre.getToolkit().beep();
                e.consume();
                JOptionPane.showMessageDialog(null, "NO DIGITE NUMERO");
            }
        }
        
        if (e.getSource() == this.vregistro.txteps)  {
            char validar = e.getKeyChar();
            if (Character.isDigit(validar)) {
                this.vregistro.txteps.getToolkit().beep();
                e.consume();
                JOptionPane.showMessageDialog(null, "NO DIGITE NUMERO");
            }
         }
        
    
           
        if (e.getSource() == this.vregistro.txtapellido)  {
            char validar = e.getKeyChar();
            if (Character.isDigit(validar)) {
                 this.vregistro.txtapellido.getToolkit().beep();
                 e.consume();
                 JOptionPane.showMessageDialog(null, "NO DIGITE NUMERO");
           }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
    
    }

    @Override
    public void keyReleased(KeyEvent e) {
        
    }

    private void dispose() {
        
    }
}


