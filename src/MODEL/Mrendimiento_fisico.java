
package MODEL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Mrendimiento_fisico extends coneccion {
    
    String Nombre ;
    int Numero_de_identificacion ;
    String posicion_de_juego ;
     int  velocidad ;
     int  destreza ;
     int  agilidad ;
     int  control ;
     int  resistencia ;
     int  diciplina  ;
     int  punteria ;

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public int getNumero_de_identificacion() {
        return Numero_de_identificacion;
    }

    public void setNumero_de_identificacion(int Numero_de_identificacion) {
        this.Numero_de_identificacion = Numero_de_identificacion;
    }

    public String getPosicion_de_juego() {
        return posicion_de_juego;
    }

    public void setPosicion_de_juego(String posicion_de_juego) {
        this.posicion_de_juego = posicion_de_juego;
    }

    public int getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(int velocidad) {
        this.velocidad = velocidad;
    }

    public int getDestreza() {
        return destreza;
    }

    public void setDestreza(int destreza) {
        this.destreza = destreza;
    }

    public int getAgilidad() {
        return agilidad;
    }

    public void setAgilidad(int agilidad) {
        this.agilidad = agilidad;
    }

    public int getControl() {
        return control;
    }

    public void setControl(int control) {
        this.control = control;
    }

    public int getResistencia() {
        return resistencia;
    }

    public void setResistencia(int resistencia) {
        this.resistencia = resistencia;
    }

    public int getDiciplina() {
        return diciplina;
    }

    public void setDiciplina(int diciplina) {
        this.diciplina = diciplina;
    }

    public int getPunteria() {
        return punteria;
    }

    public void setPunteria(int punteria) {
        this.punteria = punteria;
    }
     
    
    
    public boolean Guardar() {
        PreparedStatement ps = null;
        Connection con = getConnection();

        String sql = "insert into rendimiento_fisico"
                + "(Nombre,Numero_de_Identificacion,Posicion_de_juego,velocidad,Destreza,Agilidad,Control,Resistencia,Disciplina,Punteria  )"
                + "values (?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getNumero_de_identificacion());
            ps.setString(2, getNombre());
            ps.setString(3, getPosicion_de_juego());
            ps.setInt(4, getDestreza());
            ps.setInt(5, getAgilidad());
            ps.setInt(6, getVelocidad());
            ps.setInt(7, getControl());
            ps.setInt(8, getResistencia());
            ps.setInt(9, getDiciplina());
            ps.setInt(10,getPunteria());     
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    
 }  
    
    
    public boolean eliminar (Mrendimiento_fisico Mrf){
      PreparedStatement ps =null;
      Connection con =getConnection();
      
      String sql ="DELETE FROM  rendimiento_fisico WHERE Numero_de_Identificacion =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(5,Mrf.getNumero_de_identificacion());
      ps.execute();
      return true ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }
     
  
      
      
    }
      
        public boolean buscar ( Mrendimiento_fisico Mrf )
  {
      PreparedStatement ps =null;
      ResultSet rs = null ;
      Connection con =getConnection();
      
      String sql ="SELECT * FROM  rendimiento_fisico WHERE Numero_de_Identificacion =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(1,Mrf.getNumero_de_identificacion());
      rs= ps.executeQuery();
      
      
      if (rs.next())
          {
           Mrf.setNombre(rs.getString("Nombre"));
           Mrf.setNumero_de_identificacion(rs.getInt("Numero_de_identificacion"));
           Mrf.setPosicion_de_juego(rs.getString("pocision_de_jugador"));
           Mrf.setVelocidad(Integer.parseInt( rs.getString("velocidad")));
           Mrf.setDestreza(Integer.parseInt (rs.getString("destreza")));
           Mrf.setAgilidad(Integer.parseInt(rs.getString("agilidad")));
           Mrf.setControl(Integer.parseInt(rs.getString("control")));
           Mrf.setDiciplina(Integer.parseInt(rs.getString("disiplina")));
           Mrf.setPunteria(Integer.parseInt(rs.getString("punteria")));
           
           return true ;
          }
      
      
      
      return false ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }
 }  
      
      
      
        public boolean modificar ( Mrendimiento_fisico Mrf)
  {
      PreparedStatement ps =null;
      Connection con =getConnection();
      
      String sql ="UPDATE rendimient_fisico SET nombre=?, posicion_de_juego=?, velocidad=?, agilidad=?, control=?, diciplina=?, punteria=?, destreza=? WHERE Numero_de_Identificacion =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(1, getNumero_de_identificacion());
            ps.setString(2, getNombre());
            ps.setString(3, getPosicion_de_juego());
            ps.setInt(4, getVelocidad());
            ps.setInt(5, getAgilidad());
            ps.setInt(6, getControl());
            ps.setInt(7, getDiciplina());
            ps.setInt(8, getPunteria());
            ps.setInt(9, getDestreza()); 
      ps.execute();
      return true ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }    
      
  }       
        
        
      
      
  }
    
    
    
    
    
    

