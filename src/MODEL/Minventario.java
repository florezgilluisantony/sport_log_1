
package MODEL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Minventario extends coneccion {
    int Id ;
    String Nombre ;
    String Fecha ;
    int Cantidad_de_articulos ;
    String Estado ;
    
    
    
    
    public int getId () {
        return Id ;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public int getCantidad_de_articulos() {
        return Cantidad_de_articulos;
    }

    public void setCantidad_de_articulos(int Cantidad_de_articulos) {
        this.Cantidad_de_articulos = Cantidad_de_articulos;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String Estado) {
        this.Estado = Estado;
    }
   
    
    
    
    public boolean guardar (){
        PreparedStatement ps = null;
        Connection con = getConnection();

        String sql = "insert into inventario"
                + "( Id,Nombre,Fecha,Cantidad_de_articulos,Estado,)"
                + "values (?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getId());
            ps.setString(1, getNombre());
            ps.setString(2, getFecha());
            ps.setInt(3, getCantidad_de_articulos());
            ps.setString(4, getEstado());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
    
    
     public boolean eliminar (Minventario Min){
      PreparedStatement ps =null;
      Connection con =getConnection();
      
      String sql ="DELETE FROM  inventario WHERE Id =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(5,Min.getId());
      ps.execute();
      return true ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }
     
  
  }
     
     
     
    public boolean buscar ( Minventario Min )
  {
      PreparedStatement ps =null;
      ResultSet rs = null ;
      Connection con =getConnection();
      
      String sql ="SELECT * FROM  inventario WHERE Id =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(1,Min.getId());
      rs= ps.executeQuery();
      
      
      if (rs.next())
          {
           Min.setId(rs.getInt("Id"));
           Min.setNombre(rs.getString("Nombre"));
           Min.setCantidad_de_articulos(Integer.parseInt (rs.getString("cantidad_de_articulos ")));
           Min.setFecha(rs.getString("Fecha"));
           Min.setEstado(rs.getString("Estado"));
         
           
           return true ;
          }
      
      
      
      return false ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }
 }  
     
     
    
    
    public boolean modificar ( Minventario Min)
  {
      PreparedStatement ps =null;
      Connection con =getConnection();
      
      String sql ="UPDATE inventario SET nombre=?, Apellido=?, EPS=?, Categoria=?, Edad=?, Tipo_de_Identificacion=?, Sexo=?, Posicion=? WHERE Numero_de_Identificacion =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(1, getId());
            ps.setString(2, getNombre());
            ps.setString(3, getFecha());
            ps.setString(4, getEstado());
            ps.setInt(5, getCantidad_de_articulos()); 
      ps.execute();
      return true ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }    
      
  }   

    public void setCantidad_de_articulos(String txtcantidad) {
        
    }

  
   
}
