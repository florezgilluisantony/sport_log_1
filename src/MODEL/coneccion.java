
package MODEL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class coneccion {
    private final String base = "sport_log";
    private final String user = "root";
    private final String password = "1234";
    private final String url = "jdbc:mysql://localhost:3307/" + base;
    private Connection con = null;

    public Connection getConnection () {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(this.url, this.user, this.password);
        } catch (SQLException e) {
            System.err.println(e);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(coneccion.class.getName()).log(Level.SEVERE, null, ex);
        }
        return con;
    
}   
}
