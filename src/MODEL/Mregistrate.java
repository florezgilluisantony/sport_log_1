
package MODEL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Mregistrate extends coneccion {
    private String Usuario;
    private String Nombre ;
    private String tipo_de_cargo;
    private int numero_de_documento;
    private String contraseña1;
    private String Contraseña2;

    public String getUsuario() {
        return Usuario;
    }

    public void setUsuario(String Usuario) {
        this.Usuario = Usuario;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getTipo_de_cargo() {
        return tipo_de_cargo;
    }

    public void setTipo_de_cargo(String tipo_de_cargo) {
        this.tipo_de_cargo = tipo_de_cargo;
    }

    public int getNumero_de_documento() {
        return numero_de_documento;
    }

    public void setNumero_de_documento(int numero_de_documento) {
        this.numero_de_documento = numero_de_documento;
    }

    public String getContraseña1() {
        return contraseña1;
    }

    public void setContraseña1(String contraseña1) {
        this.contraseña1 = contraseña1;
    }

    public String getContraseña2() {
        return Contraseña2;
    }

    public void setContraseña2(String Contraseña2) {
        this.Contraseña2 = Contraseña2;
    }
    
    public boolean Guardar() {
        PreparedStatement ps = null;
        Connection con = getConnection();

        String sql = "insert into usuario"
                + "(Usuario,Nombre,tipo_de_cargo,numero_de_documento,contraseña1,Contraseña2)"
                + "values (?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getUsuario());
            ps.setString(2, getNombre());
            ps.setString(3, getTipo_de_cargo());
            ps.setInt(4, getNumero_de_documento());
            ps.setString(5, getContraseña1());
            ps.setString(6, getContraseña2());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    }
    
    
    
}
