
package MODEL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Mregistro_de_jugadores extends coneccion {
    
  
   private String Nombre ;
    private String Apellido;
    private String Tipo_de_Identificacion ;
    private int Numero_de_Identificacion  ;
    private int Edad ;
    private String Sexo ;
    private String EPS ;
    private String Posicion ;
    private String Categoria ;
    
    
    
 

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getTipo_de_Identificacion() {
        return Tipo_de_Identificacion;
    }

    public void setTipo_de_Identificacion(String Tipo_de_Identificacion) {
        this.Tipo_de_Identificacion = Tipo_de_Identificacion;
    }

    public int getNumero_de_Identificacion() {
        return Numero_de_Identificacion;
    }

    public void setNumero_de_Identificacion(int Numero_de_Identificacion) {
        this.Numero_de_Identificacion = Numero_de_Identificacion;
    }

    public int getEdad() {
        return Edad;
    }

    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    public String getSexo() {
        return Sexo;
    }

    public void setSexo(String Sexo) {
        this.Sexo = Sexo;
    }

    public String getEPS() {
        return EPS;
    }

    public void setEPS(String EPS) {
        this.EPS = EPS;
    }

    public String getPosicion() {
        return Posicion;
    }

    public void setPosicion(String Posicion) {
        this.Posicion = Posicion;
    }

    public String getCategoria() {
        return Categoria;
    }

    public void setCategoria(String Categoria) {
        this.Categoria = Categoria;
    }

    public boolean Guardar() {
        PreparedStatement ps = null;
        Connection con = getConnection();

        String sql = "insert into jugador"
                + "(Nombre,Apellido,Tipo_de_Identificacion,Numero_de_Identificacion,Edad,Sexo,Posicion,EPS,Categoria  )"
                + "values (?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setInt(1, getNumero_de_Identificacion());
            ps.setString(2, getNombre());
            ps.setString(3, getApellido());
            ps.setString(4, getEPS());
            ps.setString(5, getCategoria());
            ps.setInt(6, getEdad());
            ps.setString(7, getTipo_de_Identificacion());
            ps.setString(8, getSexo());
            ps.setString(9, getPosicion());     
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
            }
        }
    
 }  
    
    
    
    
     public boolean eliminar (Mregistro_de_jugadores Mrj){
      PreparedStatement ps =null;
      Connection con =getConnection();
      
      String sql ="DELETE FROM  jugadores WHERE Numero_de_Identificacion =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(5,Mrj.getNumero_de_Identificacion ());
      ps.execute();
      return true ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }
     
  
  }
     
     
     
    public boolean buscar ( Mregistro_de_jugadores Mrj )
  {
      PreparedStatement ps =null;
      ResultSet rs = null ;
      Connection con =getConnection();
      
      String sql ="SELECT * FROM  jugadores WHERE Numero_de_Identificacion =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(1,Mrj.getNumero_de_Identificacion());
      rs= ps.executeQuery();
      
      
      if (rs.next())
          {
           Mrj.setNombre(rs.getString("Nombre"));
           Mrj.setApellido(rs.getString("Apellido"));
           Mrj.setTipo_de_Identificacion(rs.getString("Tipo_de_identificacion"));
           Mrj.setNumero_de_Identificacion(Integer.parseInt( rs.getString("Numero_de_identicicacion")));
           Mrj.setEdad(Integer.parseInt (rs.getString("Edad")));
           Mrj.setSexo(rs.getString("Sexo"));
           Mrj.setEPS(rs.getString("EPS"));
           Mrj.setPosicion(rs.getString("Posicion"));
           Mrj.setCategoria(rs.getString("Categria"));
           
           return true ;
          }
      
      
      
      return false ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }
 }  
     
     
    
    
    public boolean modificar ( Mregistro_de_jugadores Mrj)
  {
      PreparedStatement ps =null;
      Connection con =getConnection();
      
      String sql ="UPDATE jugadores SET nombre=?, Apellido=?, EPS=?, Categoria=?, Edad=?, Tipo_de_Identificacion=?, Sexo=?, Posicion=? WHERE Numero_de_Identificacion =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(1, getNumero_de_Identificacion());
            ps.setString(2, getNombre());
            ps.setString(3, getApellido());
            ps.setString(4, getEPS());
            ps.setString(5, getCategoria());
            ps.setInt(6, getEdad());
            ps.setString(7, getTipo_de_Identificacion());
            ps.setString(8, getSexo());
            ps.setInt(9, getEdad());
            ps.setString(10, getPosicion()); 
      ps.execute();
      return true ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }    
      
  }   
     
     
    
    
}
