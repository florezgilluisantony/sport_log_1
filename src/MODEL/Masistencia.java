
package MODEL;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Masistencia extends coneccion {
    
    String categoria ;
    int Id ;
    String Nombre ;
    String Apellido ;
    String Fecha;
    int documento ;
    int Edad ;
    String asistencia ;
    
    
    
    
    public int getEdad() {
        return Edad;
    }

    public void setEdad(int Edad) {
        this.Edad = Edad;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String Nombre) {
        this.Nombre = Nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String Apellido) {
        this.Apellido = Apellido;
    }

    public String getFecha() {
        return Fecha;
    }

    public void setFecha(String Fecha) {
        this.Fecha = Fecha;
    }

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public String getAsistencia() {
        return asistencia;
    }

    public void setAsistencia(String asistencia) {
        this.asistencia = asistencia;
    }
    
    
public boolean Guardar() {
        PreparedStatement ps = null;
        Connection con = getConnection();

        String sql = "insert into asistencias"
                + "(Id,categoria,Nombre,Edad,Apellido,fecha,documento,asistencia)"
                + "values (?,?,?,?,?,?,?,?)";
        try {
            ps = con.prepareStatement(sql);
            ps.setString(1, getCategoria());
            ps.setInt(2, getId());
            ps.setString(3, getNombre());
            ps.setString(4, getApellido());
            ps.setString(5, getFecha());
            ps.setInt(6, getDocumento());
            ps.setString(7, getAsistencia()); 
            ps.setInt(6, getEdad());
            ps.execute();
            return true;
        } catch (SQLException e) {
            System.err.println(e);
            return false;
        } finally {
            try {
                con.close();
            } catch (SQLException e) {
                System.err.println(e);
                
                
            }
        }
    
 }      
    
    
    

     public boolean eliminar (Masistencia Mas){
      PreparedStatement ps =null;
      Connection con =getConnection();
      
      String sql ="DELETE FROM  asistencias WHERE Id =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(5,Mas.getId());
      ps.execute();
      return true ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }
     
  
  }    
    
    
   
   public boolean buscar ( Masistencia Mas )
  {
      PreparedStatement ps =null;
      ResultSet rs = null ;
      Connection con =getConnection();
      
      String sql ="SELECT * FROM  asistencia WHERE Id =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(1,Mas.getId());
      rs= ps.executeQuery();
      
      
      if (rs.next())
          {
           Mas.setId(rs.getInt("Id"));
           Mas.setNombre(rs.getString("Nombre"));
           Mas.setApellido(rs.getString("Nombre"));
           Mas.setDocumento(Integer.parseInt (rs.getString("cantidad_de_articulos ")));
           Mas.setCategoria(rs.getString("Categoria"));
           Mas.setFecha(rs.getString("Fecha"));
           Mas.setEdad(Integer.parseInt(rs.getString("Estado")));
           Mas.setAsistencia(rs.getString("Asistencia"));
           
           return true ;
          }
      
      
      
      return false ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }
 }    
   
   
   
   
    public boolean modificar ( Masistencia Mas)
  {
      PreparedStatement ps =null;
      Connection con =getConnection();
      
      String sql ="UPDATE asistencia SET categoria=?,Nombre=?,Edad=?,Apellido=?,fecha=?,documento=?,asistencia=? WHERE Id =? ";
      
      try{
          
      ps = con.prepareStatement(sql);
      ps.setInt(1, getId());
            ps.setString(2, getNombre());
            ps.setInt(3, getEdad());
            ps.setString(4, getApellido());
            ps.setString(5, getFecha()); 
            ps.setInt(6, getDocumento());
            ps.setString(7, getAsistencia());
            ps.setString(8, getCategoria()); 
      ps.execute();
      return true ;
      
      
      } catch (SQLException e){
      
          System.err.println(e);
          return false ;
          
      } finally {
          try {
          con.close();
          
          }catch (SQLException e){
          
           System.err.println(e);
            
          
         }
     }    
      
  }
     
     
    
    
    
    
}
